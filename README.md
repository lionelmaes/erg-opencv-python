# erg-opencv-python

A set of demos for features detection in webcam stream, adapted from existing projects. 
This is a teaching project for the "Pratiques numériques" class at ERG (École de Recherche Graphique, Brussels)
